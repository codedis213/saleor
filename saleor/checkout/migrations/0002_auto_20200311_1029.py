# Generated by Django 3.0.4 on 2020-03-11 15:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('product', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('giftcard', '0001_initial'),
        ('account', '0001_initial'),
        ('checkout', '0001_initial'),
        ('shipping', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='checkoutline',
            name='variant',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='product.ProductVariant'),
        ),
        migrations.AddField(
            model_name='checkout',
            name='billing_address',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='account.Address'),
        ),
        migrations.AddField(
            model_name='checkout',
            name='gift_cards',
            field=models.ManyToManyField(blank=True, related_name='checkouts', to='giftcard.GiftCard'),
        ),
        migrations.AddField(
            model_name='checkout',
            name='shipping_address',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='account.Address'),
        ),
        migrations.AddField(
            model_name='checkout',
            name='shipping_method',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='checkouts', to='shipping.ShippingMethod'),
        ),
        migrations.AddField(
            model_name='checkout',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='checkouts', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='checkoutline',
            unique_together={('checkout', 'variant', 'data')},
        ),
    ]
