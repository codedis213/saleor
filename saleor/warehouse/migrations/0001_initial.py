# Generated by Django 3.0.4 on 2020-03-11 15:29

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('account', '0001_initial'),
        ('product', '0001_initial'),
        ('shipping', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Warehouse',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=250)),
                ('slug', models.SlugField(max_length=255, unique=True)),
                ('company_name', models.CharField(blank=True, max_length=255)),
                ('email', models.EmailField(blank=True, default='', max_length=254)),
                ('address', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='account.Address')),
                ('shipping_zones', models.ManyToManyField(blank=True, related_name='warehouses', to='shipping.ShippingZone')),
            ],
            options={
                'ordering': ('-name',),
            },
        ),
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(default=0)),
                ('quantity_allocated', models.PositiveIntegerField(default=0)),
                ('product_variant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='stocks', to='product.ProductVariant')),
                ('warehouse', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='warehouse.Warehouse')),
            ],
            options={
                'unique_together': {('warehouse', 'product_variant')},
            },
        ),
    ]
