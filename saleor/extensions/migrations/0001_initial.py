# Generated by Django 3.0.4 on 2020-03-11 15:29

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PluginConfiguration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, unique=True)),
                ('description', models.TextField(blank=True)),
                ('active', models.BooleanField(default=True)),
                ('configuration', jsonfield.fields.JSONField(blank=True, default=dict, null=True)),
            ],
            options={
                'permissions': (('manage_plugins', 'Manage plugins'),),
            },
        ),
    ]
